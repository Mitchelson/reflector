// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BeamProjectCharacter.generated.h"

class UInputComponent;

UCLASS(config=Game)
class ABeamProjectCharacter : public ACharacter
{
    GENERATED_BODY()
    
    /** First person camera */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* FirstPersonCameraComponent;
    
    
public:
    ABeamProjectCharacter();
    
protected:
    virtual void BeginPlay();
    
public:
    /** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
    float BaseTurnRate;
    
    /** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
    float BaseLookUpRate;
    
    
protected:
    
    /** Fires. */
    void OnFire();
    
    /** Handles moving forward/backward */
    void MoveForward(float Val);
    
    /** Handles stafing movement, left and right */
    void MoveRight(float Val);
    
    /**
     * Called via input to turn at a given rate.
     * @param Rate    This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
     */
    void TurnAtRate(float Rate);
    
    /**
     * Called via input to turn look up/down at a given rate.
     * @param Rate    This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
     */
    void LookUpAtRate(float Rate);
    
protected:
    // APawn interface
    virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
    
    void LineTrace(FVector startVector, FVector endVector);
    
public:
    
    virtual void Tick(float DeltaTime) override;
};

