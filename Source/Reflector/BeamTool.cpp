// Mitchelson Brooks

#include "BeamTool.h"
#include "DrawDebugHelpers.h"

#define BEAM_RANGE 2000.0

// ABeamTool

ABeamTool::ABeamTool()
{
    ToolMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Beam Tool Mesh"));
    RootComponent = ToolMesh;

    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.bStartWithTickEnabled = true;
}

void ABeamTool::BeginPlay()
{
    // Call the base class
    Super::BeginPlay();
}

void ABeamTool::LineTrace(FVector startVector, FVector endVector)
{
    FColor traceColor = FColor::Red;
    FHitResult hit;
    FCollisionQueryParams CollisionParams;
    CollisionParams.AddIgnoredActor(this);

    GetWorld()->LineTraceSingleByChannel(hit,startVector, endVector, ECC_PhysicsBody, CollisionParams);

    if (hit.bBlockingHit)
    {
        if(hit.Actor->ActorHasTag(FName("reflectorPanel")))
        {
            UE_LOG(LogTemp, Display, TEXT("Reflector Panel has been hit, reflecting new beam"));

            traceColor = FColor::Green;

            FVector reflectedVector = FMath::GetReflectionVector((hit.Location - hit.TraceStart), hit.ImpactNormal);
            LineTrace(hit.Location, hit.Location + (reflectedVector * BEAM_RANGE));
        }

       endVector = hit.Location;
    }
    else
    {
      UE_LOG(LogTemp, Warning, TEXT("BEAM DIDN'T HIT ANYTHING"));
    }

    DrawDebugLine(GetWorld(), startVector, endVector, traceColor, false, 1.0f, 0, 8.0f);
}

void ABeamTool::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    LineTrace(ToolMesh->GetComponentLocation(), ToolMesh->GetComponentLocation() + (ToolMesh->GetForwardVector() * BEAM_RANGE) );
}
