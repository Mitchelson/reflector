// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "BeamProjectCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "DrawDebugHelpers.h"

#define BEAM_RANGE 2000.0

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// ABeamProjectCharacter

ABeamProjectCharacter::ABeamProjectCharacter()
{
    // Set size for collision capsule
    GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);
    
    // set our turn rates for input
    BaseTurnRate = 45.f;
    BaseLookUpRate = 45.f;
    
    // Create a CameraComponent
    FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
    FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
    FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
    FirstPersonCameraComponent->bUsePawnControlRotation = true;
}

void ABeamProjectCharacter::BeginPlay()
{
    // Call the base class
    Super::BeginPlay();
}

//////////////////////////////////////////////////////////////////////////
// Input

void ABeamProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
    // set up gameplay key bindings
    check(PlayerInputComponent);
    
    // Bind jump events
    PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
    PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
    
    // Bind fire event
    PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ABeamProjectCharacter::OnFire);
    
    // Bind movement events
    PlayerInputComponent->BindAxis("MoveForward", this, &ABeamProjectCharacter::MoveForward);
    PlayerInputComponent->BindAxis("MoveRight", this, &ABeamProjectCharacter::MoveRight);
    
    // We have 2 versions of the rotation bindings to handle different kinds of devices differently
    // "turn" handles devices that provide an absolute delta, such as a mouse.
    // "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
    PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
    PlayerInputComponent->BindAxis("TurnRate", this, &ABeamProjectCharacter::TurnAtRate);
    PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
    PlayerInputComponent->BindAxis("LookUpRate", this, &ABeamProjectCharacter::LookUpAtRate);
}

void ABeamProjectCharacter::OnFire()
{
    LineTrace(FirstPersonCameraComponent->GetComponentLocation(), FirstPersonCameraComponent->GetComponentLocation() + (FirstPersonCameraComponent->GetForwardVector() * 1500.0f) );
}


void ABeamProjectCharacter::MoveForward(float Value)
{
    if (Value != 0.0f)
    {
        // add movement in that direction
        AddMovementInput(GetActorForwardVector(), Value);
    }
}

void ABeamProjectCharacter::MoveRight(float Value)
{
    if (Value != 0.0f)
    {
        // add movement in that direction
        AddMovementInput(GetActorRightVector(), Value);
    }
}

void ABeamProjectCharacter::TurnAtRate(float Rate)
{
    // calculate delta for this frame from the rate information
    AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ABeamProjectCharacter::LookUpAtRate(float Rate)
{
    // calculate delta for this frame from the rate information
    AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ABeamProjectCharacter::LineTrace(FVector startVector, FVector endVector)
{
    FColor traceColor = FColor::Green;
    FHitResult hit;
    FCollisionQueryParams CollisionParams;
    CollisionParams.AddIgnoredActor(this);
    GetWorld()->LineTraceSingleByChannel(hit,startVector, endVector, ECC_Camera, CollisionParams);
    
    if (hit.bBlockingHit)
    {
        UE_LOG(LogTemp, Warning, TEXT("Beam Hit: %s"), *hit.GetComponent()->GetName());
        
        if(hit.Actor->ActorHasTag(FName("reflectorPanel")))
        {
            traceColor = FColor::Cyan;
            
            UE_LOG(LogTemp, Display, TEXT("Reflector Panel has been hit, reflecting new beam"));
            
            FVector reflectedVector = FMath::GetReflectionVector((FirstPersonCameraComponent->GetForwardVector() * BEAM_RANGE), hit.ImpactNormal);
            
            LineTrace(hit.ImpactPoint + hit.ImpactNormal, hit.ImpactPoint + reflectedVector);
        }
        
        endVector = hit.Location;
    }
    else
    {
        traceColor = FColor::Red;
    }
    
    
    DrawDebugLine(GetWorld(), hit.TraceStart, endVector, traceColor, false, 1.0f, 0, 4.0f);
}

void ABeamProjectCharacter::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    
    LineTrace(FirstPersonCameraComponent->GetComponentLocation(), FirstPersonCameraComponent->GetComponentLocation() + (FirstPersonCameraComponent->GetForwardVector() * BEAM_RANGE) );
}
