// Mitchelson Brooks

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BeamTool.generated.h"

UCLASS()
class REFLECTOR_API ABeamTool : public AActor
{
    GENERATED_BODY()

public:

    ABeamTool();

    virtual void Tick(float DeltaTime) override;

protected:

	 virtual void BeginPlay() override;

    UPROPERTY(VisibleAnywhere)
    UStaticMeshComponent* ToolMesh;

protected:

    void LineTrace(FVector startVector, FVector endVector);

};
